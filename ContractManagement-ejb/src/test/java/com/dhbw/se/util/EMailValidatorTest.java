/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.dhbw.se.util;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Philipp
 */
public class EMailValidatorTest {
    
    public EMailValidatorTest() {
    }
    
    @Test
    public void testCorrect(){
        boolean testresult = true;
        EMailValidator testval = new EMailValidator();   
        assertEquals(testresult, testval.validate("test.test@test.de"));
    }
    @Test
    public void testnodomain(){
        boolean testresult = false;
        EMailValidator testval = new EMailValidator();
        assertEquals(testresult, testval.validate("test@test"));
    }
    public void testnoatsign(){
        boolean testresult = false;
        EMailValidator testval = new EMailValidator();
        assertEquals(testresult, testval.validate("testtest.de"));
    }
    public void testtwodomain(){
        boolean testresult = false;
        EMailValidator testval = new EMailValidator();
        assertEquals(testresult, testval.validate("test@test.de.de"));
    }
}
