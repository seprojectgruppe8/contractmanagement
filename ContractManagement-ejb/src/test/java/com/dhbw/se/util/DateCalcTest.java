package com.dhbw.se.util;

import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Philipp
 */
public class DateCalcTest {
    
    public DateCalcTest() {
    }
    
    /**
     * Test of calcCancelationDate method, of class DateCalc.
     */
    @Test
    public void testCalcCancelationDate() {
        System.out.println("calcCancelationDate");
        Date start = new Date(0);
        int span = 1;
        int deadline = 3;
        Date expResult = new Date(259200000);
        Date result = DateCalc.calcCancelationDate(start, span, deadline);
        assertEquals(expResult, result);
        start = new Date(918291283);
        expResult = new Date(259200000+918291283);
        result = DateCalc.calcCancelationDate(start, span, deadline);
        assertEquals(expResult, result);        
    }
}