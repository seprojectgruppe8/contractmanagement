/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.mailer;

import com.dhbw.se.model.Users;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Creates and sends an E-Mail message
 * @author Dominique
 */
public class Newsletter {

    /**
     * 
     */
    public Newsletter() {
    }

    /**
     * Creates and sends E-Mail with the comitted contract number and the comitted description
     * @param user
     * @param number
     * @param description
     */
    public void sendNewsletter(Users user, int number, String description) {
        final String username = "vertragsmanagement.noreply@gmail.com";
        final String password = "aststinkt";
        
        System.out.println("okay");
        
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("vertragsmanagement.noreply@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(user.getEmail()));
            message.setSubject("Vertragsmanagement - Info");
            message.setText("Hallo "+user.getPrename()+" "+user.getName()+", \r\n\r\nwir wollen Sie darauf hinweisen, dass die Kündigungsfrist für ihr Vertrag '"+description+"' mit der Nummer '"+number+"' in 7 Tagen fällig ist. \r\n\r\nMit freundlichen Grüßen, \r\nIhr Vertragsmanagement-Team!");
            Transport.send(message);
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
