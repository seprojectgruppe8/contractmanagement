/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Validates E-Mail adresses
 * @author Philipp
 */
public class EMailValidator {

    private final String expr;
    private Pattern mailpattern;
    private Matcher mailmatcher;
    
    /**
     * Creates a EMail Validator object
     */
    public EMailValidator(){
        this.expr = "^[A-Za-z0-9.!#$%&'*+-/=?^_`{|}~][A-Za-z0-9.!#$%&'*+-/=?^_`{|}~.]*+@[A-Za-z0-9]+[.]{1}+[a-z]{2,3}?";
        mailpattern = Pattern.compile(expr);
    }
 
    /**
     * Validates the comitted EMail adress
     * @param mail
     * @return true or false
     */
    public boolean validate(String mail){
        mailmatcher = mailpattern.matcher(mail);
        return mailmatcher.matches();
    }
    
}
