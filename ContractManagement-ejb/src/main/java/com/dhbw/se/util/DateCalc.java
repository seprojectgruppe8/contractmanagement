/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.util;

import java.util.Date;
import java.util.Calendar;



/**
 * 
 * @author Philipp
 */
public class DateCalc {
    
    /**
     * Calculates the Date a week before the deadline is reached
     * @param start
     * @param span
     * @param deadline
     * @return calculated date
     */
    public static Date calcCancelationDate(Date start, int span, int deadline)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        cal.add(Calendar.MONTH, span);
        cal.add(Calendar.WEEK_OF_YEAR, -(deadline));
        cal.add(Calendar.DAY_OF_YEAR, -7);
        return cal.getTime();
    }
    
    
}
