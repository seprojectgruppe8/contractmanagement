/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.util;

/**
 * 
 * @author Dominique
 */
public class nullToEmptyString {

    /**
     * Converts null into an empty String
     * @param obj
     * @return empty String
     */
    public static String nullToEmptyStr(Object obj) {
        String str = (String) obj;
        if (str == null) {
            return "";
        }
        return str;
    }
}
