/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.controller;

import com.dhbw.se.model.Contract;
import com.dhbw.se.model.Supplier;
import com.dhbw.se.model.Users;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * ManagementBean handles all Processes linked to the Contract Object/Entity
 * @author Philipp
 */
@Stateless
public class ManagementBean implements ManagementBeanLocal {
    
    @PersistenceContext
    EntityManager em;
    
    /**
     * Creates an Contract and saves it to the database
     * @param user
     * @param number
     * @param type
     * @param sup
     * @param description
     * @param startDate
     * @param minimumTerm
     * @param status
     * @param cancelTime
     * @return created Contract
     */
    @Override
    public Contract createContract(Users user, int number, String type, Supplier sup, String description,Date startDate, int minimumTerm, int status, int cancelTime) {
        Contract newCon = new Contract(number, type, description, startDate, minimumTerm, status, cancelTime);
        newCon.setUserId(user);
        newCon.setSupplierId(sup);
        em.persist(newCon);
        em.flush();
        return newCon;
    }
    
    /**
     * Edits the comitted Contract with the comitted Attributes and saves the changes to the database
     * @param contract
     * @param number
     * @param type
     * @param sup
     * @param description
     * @param startDate
     * @param minimumTerm
     * @param status
     * @param cancelTime
     * @return edited contract
     */
    @Override
    public Contract changeContract(Contract contract, int number, String type, Supplier sup, String description, Date startDate, int minimumTerm, int status, int cancelTime) {
        contract.setNumber(number);
        contract.setMinimumTerm(minimumTerm);
        contract.setStatus(status);
        contract.setStartDate(startDate);
        contract.setDescription(description);
        contract.setType(type);
        contract.setSupplierId(sup);
        contract.setcancelTime(cancelTime);
        em.merge(contract);
        em.flush();
        return contract;
    }

    /**
     * Deletes the comitted contract
     * @param contract
     */
    @Override
    public void deleteContract(Contract contract) {
        contract = em.find(Contract.class, contract.getId());
        em.remove(contract);
        em.flush();
    }
    
    /**
     * Gets all running Contracts linked to the comitted User
     * @param user
     * @return Collection of Contracts
     */
    @Override
    public Collection<Contract> getRunningContracts(Users user)
    {   
        user = em.find(Users.class, user.getId());
        em.refresh(user);
        List mylist = em.createQuery("SELECT c FROM Contract c WHERE c.status = :status AND c.userId = :userid", Contract.class).setParameter("status",0).setParameter("userid", user).getResultList();
        Collection col =  new ArrayList<Supplier> (mylist);
        return col;
    }
    
    /**
     * Gets all Running contracts linked to comitted User limited by a comitted search term
     * @param user
     * @param search
     * @return Collection of Contracts
     */
    @Override
    public Collection<Contract> getRunningContractsBySearch(Users user, String search) {
        user = em.find(Users.class, user.getId());
        em.refresh(user);
        List mylist = em.createQuery("SELECT c FROM Contract c WHERE (c.status = :status AND c.userId = :userid) AND (CAST(c.number AS char(45)) LIKE :searchvalue OR c.type LIKE :searchvalue OR c.description LIKE :searchvalue OR CAST(c.startDate  AS char(45)) LIKE :searchvalue OR CAST(c.supplierId.name AS char(45)) LIKE :searchvalue OR CAST(c.cancelTime  AS char(45)) LIKE :searchvalue OR CAST(c.minimumTerm  AS char(45)) LIKE :searchvalue)", Contract.class).setParameter("status",0).setParameter("userid", user).setParameter("searchvalue", "%"+search+"%").getResultList();
        Collection col =  new ArrayList<Contract> (mylist);
        return col;
    }
    
    /**
     * Gets all contracts linked to comitted User
     * @param user
     * @return Collection of Contracs
     */
    @Override
    public Collection<Contract> getContracts(Users user) {
        user = em.find(Users.class, user.getId());
        em.refresh(user);
        return user.getContractCollection();
    }
    
    /**
     * Gets all contract linked to comitted User limited by comitted search term
     * @param user
     * @param search
     * @return Collection of Contracts
     */
    @Override
    public Collection<Contract> getContractsBySearch(Users user, String search) {
        user = em.find(Users.class, user.getId());
        em.refresh(user);
        List mylist = em.createQuery("SELECT c FROM Contract c WHERE c.userId = :userid AND (CAST(c.number AS char(45)) LIKE :search OR c.type LIKE :search OR c.description LIKE :search OR CAST(c.startDate AS char(45)) LIKE :search OR CAST(c.supplierId.name AS char(45)) LIKE :search OR CAST(c.cancelTime AS char(45)) LIKE :search OR CAST(c.minimumTerm AS char(45)) LIKE :search)", Contract.class).setParameter("userid", user).setParameter("search", "%"+search+"%").getResultList();
        Collection col =  new ArrayList<Contract> (mylist);
        return col;
    }
    
    /**
     * Gets all Suppliers saved in the database
     * @return Collection of Suppliers
     */
    @Override
    public Collection<Supplier> getSuppliers() {
        List mylist = em.createNamedQuery("Supplier.findAll", Supplier.class).getResultList();
        Collection col =  new ArrayList<Supplier> (mylist);
        return col;
    }
    
    /**
     * Gets Suppliers with the comitted ID
     * @param id
     * @return Supplier
     */
    @Override
    public Supplier getSupplierById(Integer id) {
        try {
            Supplier sup = em.createNamedQuery("Supplier.findById", Supplier.class).setParameter("id",id).getSingleResult();
            return sup;
        }
        catch  (NoResultException nre) {
            return null;
        }
        
    }
    
    /**
     * Gets Contract with the comitted ID
     * @param id
     * @return Contract
     */
    @Override
    public Contract getContractById(Integer id) {
        try {Contract con = em.createNamedQuery("Contract.findById", Contract.class).setParameter("id", id).getSingleResult();
        return con; }
        catch(NoResultException nre) {
            return null;
        }
    }
}
