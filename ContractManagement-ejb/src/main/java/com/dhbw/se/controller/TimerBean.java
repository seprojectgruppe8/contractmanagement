/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.controller;

import com.dhbw.se.mailer.Newsletter;
import com.dhbw.se.model.Contract;
import com.dhbw.se.model.Users;
import com.dhbw.se.util.DateCalc;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Timer;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;


/**
 * TimerBean checks if a contracts Cancellation Period ends within a week
 * @author Dominique
 */
@Singleton
public class TimerBean {

    @PersistenceContext
    EntityManager em;

    /**
     * checks if a contracts Cancellation Period ends within a week and sends E-Mail to inform the User
     */
    @Schedule(dayOfWeek = "*", hour="11", minute="30")
    public void runTimer() {
        try {
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            today = sdf.parse(sdf.format(today));
            List<Contract> mylist = em.createNamedQuery("Contract.findByStatus").setParameter("status", 0).getResultList();
            for (Contract c : mylist) {
                Date calcDate = DateCalc.calcCancelationDate(c.getStartDate(), c.getMinimumTerm(), c.getcancelTime());
                if (today.compareTo(calcDate) == 0) {
                    Users user = c.getUserId();
                    Newsletter email = new Newsletter();
                    email.sendNewsletter(user, c.getNumber(), c.getDescription());
                }
            }
        } catch (NoResultException | ParseException nre) {
            nre.printStackTrace();
        }

    }
}
