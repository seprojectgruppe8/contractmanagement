
package com.dhbw.se.controller;

import com.dhbw.se.model.Contract;
import com.dhbw.se.model.Supplier;
import com.dhbw.se.model.Users;
import java.util.Collection;
import java.util.Date;
import javax.ejb.Local;

/**
 * Local Interface for ManagementBean
 * @author Philipp
 */
@Local
public interface ManagementBeanLocal {
    /**
     * Creates an Contract and saves it to the database
     * @param user
     * @param number
     * @param type
     * @param sup
     * @param description
     * @param startDate
     * @param minimumTerm
     * @param status
     * @param cancelTime
     * @return created Contract
     */
    public Contract createContract(Users user, int number, String type, Supplier sup, String description,Date startDate, int minimumTerm, int status, int cancelTime); 
    /**
     * Edits the comitted Contract with the comitted Attributes and saves the changes to the database
     * @param contract
     * @param number
     * @param type
     * @param sup
     * @param description
     * @param startDate
     * @param minimumTerm
     * @param status
     * @param cancelTime
     * @return edited contract
     */
    public Contract changeContract(Contract contract, int number, String type, Supplier sup, String description, Date startDate, int minimumTerm, int status, int cancelTime);

    /**
     * Deletes the comitted contract
     * @param contract
     */
    public void deleteContract(Contract contract);    
    /**
     * Gets all running Contracts linked to the comitted User
     * @param user
     * @return Collection of Contracts
     */
    public Collection<Contract> getRunningContracts(Users user);
    /**
     * Gets all Running contracts linked to comitted User limited by a comitted search term
     * @param user
     * @param search
     * @return Collection of Contracts
     */
    public Collection<Contract> getRunningContractsBySearch(Users user, String search);    
    /**
     * Gets all contracts linked to comitted User
     * @param user
     * @return Collection of Contracs
     */
    public Collection<Contract> getContracts(Users user);
    /**
     * Gets all contract linked to comitted User limited by comitted search term
     * @param user
     * @param search
     * @return Collection of Contracts
     */
    public Collection<Contract> getContractsBySearch(Users user, String search);
    /**
     * Gets all Suppliers saved in the database
     * @return Collection of Suppliers
     */    
    public Collection<Supplier> getSuppliers();
    /**
     * Gets Suppliers with the comitted ID
     * @param id
     * @return Supplier
     */
    public Supplier getSupplierById(Integer id);    
    /**
     * Gets Contract with the comitted ID
     * @param id
     * @return Contract
     */
    public Contract getContractById(Integer id);    
}
