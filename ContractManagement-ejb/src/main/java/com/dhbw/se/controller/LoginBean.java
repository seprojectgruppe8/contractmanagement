/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.controller;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import com.dhbw.se.model.Users;
import javax.persistence.NoResultException;

/**
 * This Class handles the Login Process and handles Changes of User Data
 * @author Maurice
 */
@Stateless
public class LoginBean implements LoginBeanLocal {

    @PersistenceContext
    EntityManager em;

    /**
     * This method creates a user object 
     * @param name
     * @param prename
     * @param email
     * @param password
     * @return created user object
     */
    @Override
    public Users createUser(String name, String prename, String email, String password) {
        Users user = new Users(email, password, name, prename);
        em.persist(user);
        em.flush();
        return user;
    }

    /**
     * Checks if an User exists in the database with the comitted email and matches the comitted Password with the password in the database.
     * @param email
     * @param password
     * @return null (if User not found or Password doesn't match) or the User in the Database whose EMail matches with the comitted
     */
    @Override
    public Users checkLogin(String email, String password) {
        try {
            Users user = (Users) em.createNamedQuery("Users.findByEmail", Users.class).setParameter("email", email).getSingleResult();
            if (user.getPw().equals(password)) {
                return user;
            } else {
                return null;
            }
        } catch (NoResultException nre) {
            return null;
        }
    }

    /**
     * Checks if an User exists in the database with the comitted email
     * @param email
     * @return null(if no user is found) or the User in the Database whose EMail matches with the comitted
     */
    @Override
    public boolean checkEmail(String email) {
        try {
            em.createNamedQuery("Users.findByEmail", Users.class).setParameter("email", email).getSingleResult();
            return true;
        } catch (NoResultException nre) {
            return false;
        }
    }

    /**
     * Checks if the comitted password matches the password of the comitted User
     * @param user
     * @param password
     * @return true or false
     */
    @Override
    public boolean checkPassword(Users user, String password) {
        if (user.getPw().equals(password)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Changes the comitted Attributes of the comitted User and saves the changes in the Database
     * @param user
     * @param email
     * @param name
     * @param prename
     * @return edited User object
     */
    @Override
    public Users editUser(Users user, String email, String name, String prename) {
        //em.lock(user, LockModeType.WRITE);
        user.setEmail(email);
        user.setName(name);
        user.setPrename(prename);
        em.merge(user);
        em.flush();
        return user;
    }

    /**
     * Changes the comitted Attributes of the comitted User and saves the changes in the Database
     * @param user
     * @param email
     * @param name
     * @param prename
     * @param password
     * @return Edited User Object
     */
    @Override
    public Users editUser(Users user, String email, String name, String prename, String password) {
        //em.lock(user, LockModeType.WRITE);
        user.setEmail(email);
        user.setName(name);
        user.setPrename(prename);
        user.setPw(password);
        em.merge(user);
        em.flush();
        return user;
    }
    
}
