/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.controller;

import javax.ejb.Local;
import com.dhbw.se.model.Users;


/**
 * Local Interface for LoginBean
 * @author Maurice
 */
@Local
public interface LoginBeanLocal {
    /**
     * This method creates a user object 
     * @param name
     * @param prename
     * @param email
     * @param password
     * @return created user object
     */
    Users createUser(String name, String prename, String email, String password);
    /**
     * Checks if an User exists in the database with the comitted email and matches the comitted Password with the password in the database.
     * @param email
     * @param password
     * @return null (if User not found or Password doesn't match) or the User in the Database whose EMail matches with the comitted
     */
    Users checkLogin(String email, String password);
    /**
     * Checks if the comitted password matches the password of the comitted User
     * @param user
     * @param password
     * @return true or false
     */
    boolean checkPassword(Users user, String password);
    /**
     * Checks if an User exists in the database with the comitted email
     * @param email
     * @return null(if no user is found) or the User in the Database whose EMail matches with the comitted
     */
    boolean checkEmail(String email);
    /**
     * Changes the comitted Attributes of the comitted User and saves the changes in the Database
     * @param user
     * @param email
     * @param name
     * @param prename
     * @param password
     * @return Edited User Object
     */
    Users editUser(Users user, String email, String name, String prename);
    /**
     * Changes the comitted Attributes of the comitted User and saves the changes in the Database
     * @param user
     * @param email
     * @param name
     * @param prename
     * @param password
     * @return Edited User Object
     */
    Users editUser(Users user, String email, String name, String prename, String password);
}
