/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dhbw.se.controller;

import com.dhbw.se.model.Contract;
import com.dhbw.se.model.Supplier;
import com.dhbw.se.util.EMailValidator;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import com.dhbw.se.model.Users;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

/**
 * Controller Class for HTTP Requests
 * @author Maurice, Philipp
 */
@WebServlet(name = "ControllerServlet", urlPatterns = {"/Controller"})
public class ControllerServlet extends HttpServlet {

    @EJB
    LoginBeanLocal loginbean;
    @EJB
    ManagementBeanLocal mbean;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws NullPointerException  
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NullPointerException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            session = request.getSession(true);
        }
        String currentStep = request.getParameter("step");
        Users user;
        if (currentStep == null) {
            if (session.getAttribute("user") != null) {
                if (request.getParameter("check") != null || request.getParameter("search") != null) {
                    if (request.getParameter("check") != null && request.getParameter("search") != null) {
                        System.out.println("check and search");
                        request.setAttribute("checked", "checked");
                        request.setAttribute("searchvalue", "value='" + request.getParameter("search") + "'");
                        session.setAttribute("contracts", mbean.getContractsBySearch((Users) session.getAttribute("user"), request.getParameter("search")));
                        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                    } else if (request.getParameter("check") != null) {
                        if (session.getAttribute("user") != null) {
                            request.setAttribute("checked", "checked");
                            session.setAttribute("contracts", mbean.getContracts((Users) session.getAttribute("user")));
                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                        }
                    } else if (request.getParameter("search") != null) {
                        System.out.println(request.getParameter("search"));
                        request.setAttribute("searchvalue", "value='" + request.getParameter("search") + "'");
                        session.setAttribute("contracts", mbean.getRunningContractsBySearch((Users) session.getAttribute("user"), request.getParameter("search")));
                        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                    } else {
                        session.setAttribute("contracts", getContractsByUser((Users) session.getAttribute("user")));
                        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                    }
                } else {
                    session.setAttribute("contracts", getContractsByUser((Users) session.getAttribute("user")));
                    request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                }
            } else {
                request.getRequestDispatcher("/login.jsp").forward(request, response);
            }
        } else {
            switch (currentStep) {
                case "login":
                    if (session.getAttribute("user") == null) {
                        user = loginbean.checkLogin(request.getParameter("email"), request.getParameter("password"));
                        if (user != null) {
                            //session = request.getSession(true);
                            session.setAttribute("user", user);
                            session.setAttribute("contracts", getContractsByUser((Users) session.getAttribute("user")));
                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                        } else {
                            request.setAttribute("message", "Falsche Email/Falsches Passwort! Bitte Eingabe wiederholen!");
                            request.getRequestDispatcher("/login.jsp").forward(request, response);
                        }
                    } else {
                        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                    }
                    break;
                case "logout":
                    session.invalidate();
                    request.setAttribute("message", "Erfolgreich ausgelogged!");
                    request.getRequestDispatcher("/login.jsp").forward(request, response);
                    break;
                case "register":
                    request.getRequestDispatcher("/register.jsp").forward(request, response);
                    break;
                case "save":
                    switch (request.getParameter("submitbutton")) {
                        case "Registrieren":
                            EMailValidator mailval = new EMailValidator();
                            if (mailval.validate(request.getParameter("email"))) {
                                if (loginbean.checkEmail(request.getParameter("email")) == false) {
                                    if (request.getParameter("password1") != null && request.getParameter("password2") != null && request.getParameter("password1").equals(request.getParameter("password2"))) {
                                        user = loginbean.createUser(request.getParameter("name"), request.getParameter("prename"), request.getParameter("email"), request.getParameter("password1"));
                                        if (user != null) {
                                            //session = request.getSession(true);
                                            session.setAttribute("user", user);
                                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                                        } else {
                                            request.setAttribute("message", "Ein User mit dieser Email existiert bereits!");
                                            request.getRequestDispatcher("/register.jsp").forward(request, response);
                                        }
                                    } else {
                                        request.setAttribute("message", "Passwort stimmt nicht \374berein!");
                                        request.getRequestDispatcher("/register.jsp").forward(request, response);
                                    }
                                } else {
                                    request.setAttribute("message", "Die eingegebene E-Mail Adresse ist bereits vergeben");
                                    request.getRequestDispatcher("/register.jsp").forward(request, response);
                                }
                            } else {
                                request.setAttribute("message", "Die eingegebene E-Mail Adresse ist keine zulässige Adresse");
                                request.getRequestDispatcher("/register.jsp").forward(request, response);
                            }
                            break;
                        case "Update User":
                            if (session.getAttribute("user") != null) {
                                if (request.getParameter("password").isEmpty() == false) {
                                    if (loginbean.checkPassword((Users) session.getAttribute("user"), request.getParameter("password"))) {
                                        if (request.getParameter("password1").isEmpty() == false && request.getParameter("password2").isEmpty() == false && request.getParameter("password1").equals(request.getParameter("password2"))) {
                                            user = loginbean.editUser((Users) session.getAttribute("user"), request.getParameter("email"), request.getParameter("name"), request.getParameter("prename"), request.getParameter("password1"));
                                            session.setAttribute("user", user);
                                            request.setAttribute("message", "User erfolgreich editiert");
                                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                                        } else {
                                            request.setAttribute("message", "Neue Passwörter stimmen nicht überein!");
                                            request.getRequestDispatcher("/edituser.jsp").forward(request, response);
                                        }
                                    } else {
                                        request.setAttribute("message", "Altes Passwort ist falsch!");
                                        request.getRequestDispatcher("/edituser.jsp").forward(request, response);
                                    }

                                } else {
                                    if (request.getParameter("password1").isEmpty() == false || request.getParameter("password2").isEmpty() == false) {
                                        request.setAttribute("message", "Bitte altes Passwort eingeben!");
                                        request.getRequestDispatcher("/edituser.jsp").forward(request, response);
                                    } else {
                                        user = loginbean.editUser((Users) session.getAttribute("user"), request.getParameter("email"), request.getParameter("name"), request.getParameter("prename"));
                                        session.setAttribute("user", user);
                                        request.setAttribute("message", "User erfolgreich editiert");
                                        request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                                    }
                                }
                            } else {
                                request.setAttribute("message", "Session ist abgelaufen!");
                                request.getRequestDispatcher("/login.jsp").forward(request, response);
                            }
                            break;
                        case "Vertrag erstellen":
                            if (session.getAttribute("user") != null) {
                                try {
                                    int number = Integer.parseInt(request.getParameter("number"));
                                    int minimumterm = Integer.parseInt(request.getParameter("minimumterm"));
                                    int cancelTime = Integer.parseInt(request.getParameter("cancelTime"));
                                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
                                    Supplier sup = mbean.getSupplierById(Integer.parseInt(request.getParameter("supplier")));
                                    mbean.createContract((Users) session.getAttribute("user"), number, request.getParameter("type"), sup, request.getParameter("description"), date, minimumterm, 0, cancelTime);
                                    session.setAttribute("contracts", getContractsByUser((Users) session.getAttribute("user")));
                                    request.setAttribute("message", "Vertrag erfolgreich eingetragen");
                                    request.getRequestDispatcher("/welcome.jsp").forward(request, response);

                                } catch (NumberFormatException nfe) {
                                    request.setAttribute("message", "Nummer und Mindestlaufzeit müssen Integer Werte sein!");
                                    request.setAttribute("options", this.getAllSuppliers());
                                    request.getRequestDispatcher("/newcon.jsp").forward(request, response);
                                } catch (ParseException pe) {
                                    request.setAttribute("message", "Datum muss wie folgt eingegeben werden: yyyy-MM-dd");
                                    request.setAttribute("options", this.getAllSuppliers());
                                    request.getRequestDispatcher("/newcon.jsp").forward(request, response);
                                }
                            } else {
                                request.setAttribute("message", "Session ist abgelaufen!");
                                request.getRequestDispatcher("/login.jsp").forward(request, response);
                            }
                            break;
                        case "Update Vertrag":
                            if (session.getAttribute("user") != null && session.getAttribute("contract") != null) {
                                try {
                                    int number = Integer.parseInt(request.getParameter("number"));
                                    int minimumterm = Integer.parseInt(request.getParameter("minimumterm"));
                                    int status = Integer.parseInt(request.getParameter("status"));
                                    int cancelTime = Integer.parseInt(request.getParameter("cancelTime"));
                                    Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("date"));
                                    Supplier sup = mbean.getSupplierById(Integer.parseInt(request.getParameter("supplier")));
                                    mbean.changeContract((Contract) session.getAttribute("contract"), number, request.getParameter("type"), sup, request.getParameter("description"), date, minimumterm, status, cancelTime);
                                    session.setAttribute("contracts", getContractsByUser((Users) session.getAttribute("user")));
                                    request.setAttribute("message", "Vertrag erfolgreich aktualisiert!");
                                    request.getRequestDispatcher("/welcome.jsp").forward(request, response);

                                } catch (NumberFormatException nfe) {
                                    request.setAttribute("message", "Nummer und Mindestlaufzeit müssen Integer Werte sein!");
                                    request.setAttribute("options", this.getAllSuppliers());
                                    request.getRequestDispatcher("/editcon.jsp").forward(request, response);
                                } catch (ParseException pe) {
                                    request.setAttribute("message", "Datum muss wie folgt eingegeben werden: yyyy-MM-dd");
                                    request.setAttribute("options", this.getAllSuppliers());
                                    request.getRequestDispatcher("/editcon.jsp").forward(request, response);
                                }
                            } else {
                                request.setAttribute("message", "Session ist abgelaufen!");
                                request.getRequestDispatcher("/login.jsp").forward(request, response);
                            }
                            break;
                        default:
                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                            break;
                    }
                    break;
                case "edituser":
                    if (session.getAttribute("user") != null) {
                        request.getRequestDispatcher("/edituser.jsp").forward(request, response);
                    } else {
                        request.setAttribute("message", "Session ist abgelaufen!");
                        request.getRequestDispatcher("/login.jsp").forward(request, response);
                    }
                    break;
                case "newcon":
                    if (session.getAttribute("user") != null) {
                        request.setAttribute("options", this.getAllSuppliers());
                        request.getRequestDispatcher("/newcon.jsp").forward(request, response);
                    } else {
                        request.setAttribute("message", "Session ist abgelaufen!");
                        request.getRequestDispatcher("/login.jsp").forward(request, response);
                    }
                    break;
                case "editcontract":
                    if (session.getAttribute("user") != null && request.getParameter("con") != null) {
                        user = (Users) session.getAttribute("user");
                        Contract con = mbean.getContractById(Integer.parseInt(request.getParameter("con")));
                        if (con.getUserId().getId() == user.getId()) {
                            session.setAttribute("contract", con);
                            request.setAttribute("options", this.getAllSuppliers());
                            request.getRequestDispatcher("/editcon.jsp").forward(request, response);
                        } else {
                            request.setAttribute("message", "Ooops, UserId stimmt nicht mit ContractId überein!");
                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                        }
                    } else {
                        request.setAttribute("message", "Session ist abgelaufen!");
                        request.getRequestDispatcher("/login.jsp").forward(request, response);
                    }
                    break;
                case "deletecontract":
                    if (session.getAttribute("user") != null && request.getParameter("con") != null) {
                        user = (Users) session.getAttribute("user");
                        Contract con = mbean.getContractById(Integer.parseInt(request.getParameter("con")));
                        if (con != null) {
                            if (con.getUserId().getId() == user.getId()) {
                                mbean.deleteContract(con);
                                session.setAttribute("contracts", getContractsByUser((Users) session.getAttribute("user")));
                                request.setAttribute("message", "Vertrag erfolgreich gelöscht!");
                                request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                            } else {
                                request.setAttribute("message", "Ooops, UserId stimmt nicht mit ContractId überein!");
                                request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                            }
                        } else {
                            request.setAttribute("message", "Ooops, ContractId nicht mehr vorhanden!");
                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                        }
                    } else {
                        request.setAttribute("message", "Session ist abgelaufen!");
                        request.getRequestDispatcher("/login.jsp").forward(request, response);
                    }
                    break;
                default:
                    if (session != null) {
                        if (session.getAttribute("user") != null) {
                            request.setAttribute("message", "Ungültiger Übergabe-Parameter!");
                            request.getRequestDispatcher("/welcome.jsp").forward(request, response);
                        } else {
                            //request.setAttribute("message", "Session ist abgelaufen!");
                            request.setAttribute("message", "Ungültiger Übergabe-Parameter!");
                            request.getRequestDispatcher("/login.jsp").forward(request, response);
                        }
                    } else {
                        request.setAttribute("message", "Ungültiger Übergabe-Parameter!");
                        request.getRequestDispatcher("/login.jsp").forward(request, response);
                    }
                    break;

            }
        }


    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NullPointerException npe) {
            if (request.getSession(false) != null) {
                request.getSession(false).invalidate();
            }
            request.setAttribute("message", "Ungültige Parameterübergabe oder Session abgelaufen! \r\nBitte erneut anmelden...");
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }

    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NullPointerException npe) {
            if (request.getSession(false) != null) {
                request.getSession(false).invalidate();
            }
            request.setAttribute("message", "Ungültige Parameterübergabe oder Session abgelaufen! \r\nBitte erneut anmelden...");
            request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Gets the Contracts linked to the comitted User
     * @param user
     * @return Collection of User
     */
    public Collection<Contract> getContractsByUser(Users user) {
        return mbean.getRunningContracts(user);
    }

    /**
     * Gets all Suppliers saved in the database
     * @return Collection of Suppliers
     */
    public Collection<Supplier> getAllSuppliers() {
        Collection<Supplier> sup = mbean.getSuppliers();
        return sup;
    }
}
