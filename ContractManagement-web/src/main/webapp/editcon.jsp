<%-- 
    Document   : editcon
    Created on : 10.03.2014, 17:58:26
    Author     : Maurice, Dominique
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<!DOCTYPE html>
<script>
    function setSelectedValue() {
        document.getElementById("type").value = "${contract.type}";
        document.getElementById("supplier").value = "${contract.supplierId.id}";
        document.getElementById("status").value = "${contract.status}";
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Management - Edit Contract</title>
        <link href="/ContractManagement-web/css/main.css" rel="stylesheet" type="text/css">
    </head>

    <body onload="setSelectedValue()">
        <div class="container">
            <h1>Vertrag Ändern</h1>
            <form method="post" action="/ContractManagement-web/Controller?step=save">

                <p><input type="text" placeholder="Nummer" name="number" value="${contract.number}"></p>

                <p>
                    Typ: <select id="type" name="type">
                        <option value="Abonnement">Abonnement</option>
                        <option value="Festnetz und DSL">Festnetz und DSL</option>
                        <option value="Handy">Handy</option>
                        <option value="Kredit">Kredit</option>
                        <option value="KFZ">KFZ</option>
                        <option value="Mietvertrag">Mietvertrag</option>
                        <option value="Sparvertrag">Sparvertrag</option>
                        <option value="Versicherung">Versicherung</option>
                        <option value="Andere">Andere</option>
                    </select>

                </p>

                <p>
                    Anbieter: <select id="supplier" name="supplier">
                        <c:forEach items="${options}" var="option">
                            <option value="${option.id}">${option.name}</option>
                        </c:forEach>
                    </select>
                </p>

                <p><input type="text" placeholder="Beschreibung" name="description" value="${contract.description}"></p>

                <p><input type="text" placeholder="Start Datum (yyyy-MM-dd)" name="date" value="<fmt:formatDate value="${contract.startDate}" pattern="yyyy-MM-dd" />"></p>

                <p><input type="text" placeholder="Mindestlaufzeit (in Monaten)"name="minimumterm" value="${contract.minimumTerm}"></p>

                <p>
                    Status: <select id="status" name="status">
                        <option value="0">laufend</option>
                        <option value="1">gekündigt</option>
                    </select>
                </p>
                <p>
                    <input type="text" placeholder="Kündigungsfrist (in Wochen)" name="cancelTime" value="${contract.cancelTime}">
                </p>
                <h2>${message}</h2>
                <p class="submit"><input type="submit" name="submitbutton" value="Update Vertrag"></p>
            </form>
            <form action="/ContractManagement-web/Controller" method="post">
                <p class="submit">
                    <input type="submit" value="Zurück zur Übersicht">
                </p> 
            </form>
        </div>
    </body>
</html>
