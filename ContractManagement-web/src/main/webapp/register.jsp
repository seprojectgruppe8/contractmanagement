<%-- 
    Document   : register
    Created on : 04.03.2014, 12:56:40
    Author     : Maurice, Dominique
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Management - Register</title>
        <link href="/ContractManagement-web/css/main.css" rel="stylesheet" type="text/css"> 
    </head>
    <body>
        <div class="container">
            <h1>
                Registrieren
            </h1> 

            <form action="/ContractManagement-web/Controller?step=save" method="post">
                <p>
                    <input type="text" placeholder="Ihre E-Mail" name="email">
                </p> 
                <p>
                    <input type="text" placeholder="Nachname" name="name">
                </p>
                <p>
                    <input type="text" placeholder="Vorname" name="prename">
                </p>
                <p>
                    <input type="password" placeholder="Passwort" name="password1">
                </p>
                <p>
                    <input type="password" placeholder="Passwort nochmals eingeben" name="password2" >
                </p>  
                <h2>${message}</h2>
                <p class="submit">
                    <input type="submit" name="submitbutton" value="Registrieren">
                </p>
            </form>
            <form action="/ContractManagement-web/Controller" method="post">
                <p class="submit">
                    <input type="submit" value="Zurück zur Anmeldung">
                </p> 
            </form>
        </div>
    </body>
</html>
