<%-- 
    Document   : edituser
    Created on : 06.03.2014, 17:55:46
    Author     : Maurice, Dominique
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Management - Edit User</title>
        <link href="/ContractManagement-web/css/main.css" rel="stylesheet" type="text/css"> 
    </head>
    <body>
        <div class="container">
            <h1>
                Benutzerdaten ändern
            </h1> 
            <form method="post" action="/ContractManagement-web/Controller?step=save">
                <p>
                    <input type="text" placeholder="E-Mail" value="${user.email}" name="email">
                </p>
                <p>
                    <input type="text" placeholder="Nachname" value="${user.name}" name="name" >
                </p>
                <p>
                    <input type="text" placeholder="Vorname" value="${user.prename}" name="prename" >
                </p>
                <p>
                    <input type="password" placeholder="Altes Passwort" name="password">
                </p>
                <p>
                    <input type="password" placeholder="Neues Passwort" name="password1">
                </p>
                <p>
                    <input type="password" placeholder="Neues Passwort nochmals eingeben" name="password2">
                </p>
                <h2>${message}</h2>
                <p class="submit">
                    <input type="submit" name="submitbutton" value="Update User">
                </p>
            </form>

            <form action="/ContractManagement-web/Controller" method="post">
                <p class="submit">
                    <input type="submit" value="Zurück zum Dashboard">
                </p> 
            </form>
        </div>
    </body>
</html>
