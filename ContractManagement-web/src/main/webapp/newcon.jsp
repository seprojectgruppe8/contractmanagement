<%-- 
    Document   : newcon
    Created on : 07.03.2014, 17:52:29
    Author     : Maurice, Dominique
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Management - New Contract</title>
        <link href="/ContractManagement-web/css/main.css" rel="stylesheet" type="text/css"> 
    </head>
    <body>
        <div class="container">
            <h1> Vertrag erstellen </h1>

            <form action="/ContractManagement-web/Controller?step=save" method="post">
                <p>
                    <input type="text" placeholder="Nummer" name="number" >
                </p>
                <p>
                    Typ: <select name="type">
                        <option value="Abonnement">Abonnement</option>
                        <option value="Festnetz und DSL">Festnetz und DSL</option>
                        <option value="Handy">Handy</option>
                        <option value="Kredit">Kredit</option>
                        <option value="KFZ">KFZ</option>
                        <option value="Mietvertrag">Mietvertrag</option>
                        <option value="Sparvertrag">Sparvertrag</option>
                        <option value="Versicherung">Versicherung</option>
                        <option value="Andere">Andere</option>
                    </select>
                </p>
                <p>
                    Anbieter: <select name="supplier">
                        <c:forEach items="${options}" var="option">
                            <option value="${option.id}">${option.name}</option>
                        </c:forEach>
                    </select>
                </p>
                <p>
                    <input type="text" placeholder="Beschreibung" name="description">
                </p>
                <p>
                    <input type="text" placeholder="Start Datum (yyyy-MM-dd)" name="date">
                </p>
                <p>
                    <input type="text" placeholder="Mindestlaufzeit (in Monaten)" name="minimumterm">
                </p>
                <p>
                    <input type="text" placeholder="Kündigungsfrist (in Wochen)" name="cancelTime">
                </p>
                <h2>${message}</h2>
                
                <p class="submit">    
                    <input type="submit" name="submitbutton" value="Vertrag erstellen">  
                </p> 
                
            </form>
            <form action="/ContractManagement-web/Controller" method="post">
                <p class="submit">
                    <input type="submit" value="Zurück zur Übersicht">
                </p> 
            </form>
        </div>
    </body>
</html>
