<%-- 
    Document   : welcome
    Created on : 04.03.2014, 13:32:50
    Author     : Maurice, Philipp, Dominique
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Management - Welcome</title>
        <link href="/ContractManagement-web/css/main.css" rel="stylesheet" type="text/css"> 
    </head>
    <body>
        <div class="container">
            <h1>Vertragsmangement</h1>
            <h2>${message}</h2>
            <h3>Welcome ${user.prename} ${user.name}! <a title="Logout" href="/ContractManagement-web/Controller?step=logout"><img src="img/logout.png" width="25px" height="25px" /></a></h3>
            <p>eingeloggt mit: ${user.email}</p>
            <a style="float: left" title="Profil ändern" href="/ContractManagement-web/Controller?step=edituser"><img src="img/edituser.png" width="40" height="40" /> Profil ändern</a>
            <a style="margin-left: 15px; clear:left;"title="Vertrag erstellen" href="/ContractManagement-web/Controller?step=newcon"><img src="img/add.png" width="40" height="40" /> Vertrag erstellen</a>
        </div>
        <div class="container2">
            <h1>Übersicht</h1>
            <form style="margin-bottom: 20px" action="/ContractManagement-web/Controller" method="post">
                <input type="checkbox" name="check" onclick="this.form.submit()" ${checked}>Gekündigte Verträge anzeigen</input>
            <input style="margin-left: 250px" type="text" placeholder="Suchbegriff" name="search" ${searchvalue}/>
            <input type="image" src="img/search.png" alt="Absenden" width="20px" height="20px"/> 
            </form>
            <table border="2" rules="rows">
                <tr id="parentrow">
                    <td>Nummer </td>
                    <td>Typ </td>
                    <td>Anbieter </td>
                    <td>Beschreibung </td>
                    <td>Start </td>
                    <td>Mindest-<br>laufzeit </td>
                    <td>Kündigungs-<br>frist </td>
                </tr>
                <c:forEach items="${contracts}" var="con">
                    <tr id="childrow">
                        <td>${con.number}</td>
                        <td>${con.type}</td>
                        <td>${con.supplierId.name}</td>
                        <td>${con.description}</td>
                        <td><fmt:formatDate value="${con.startDate}" pattern="dd.MM.yyyy" /></td>
                        <td>${con.minimumTerm} Monat(e)</td>
                        <td>${con.cancelTime} Woche(n)</td>
                        <td><a title="ändern" href="/ContractManagement-web/Controller?step=editcontract&con=${con.id}"><img width="20px" height="20px" src="img/edit.png" /></a></td>
                        <td><a title="löschen" href="/ContractManagement-web/Controller?step=deletecontract&con=${con.id}" onclick="return window.confirm('Wollen sie den Vertrag endgültig löschen?\r\nSie können auch den Status des Vertrages auf <gekündigt> setzen um den Vertrag im Archiv zu behalten.');" ><img src="img/delete.png" width="20px" height="20px" /></a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </body>
</html>
