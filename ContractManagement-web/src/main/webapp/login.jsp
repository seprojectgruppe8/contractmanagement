<%-- 
    Document   : login
    Created on : 27.02.2014, 09:56:11
    Author     : Philipp, Dominique
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Contract Management - Login</title>
        <link href="/ContractManagement-web/css/main.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <h1>
                Vertragsmanagement
            </h1>                
            <form action="/ContractManagement-web/Controller?step=login" method="post">
                <p>
                    <input type="text" placeholder="Email" value="" name="email">
                </p>
                <p>
                    <input type="password" placeholder="Passwort" value="" name="password">
                </p>
                <h2>${message}</h2>
                <p class="submit">
                    <input type="submit" value="Anmelden" />
                </p>
            </form>
        </div>
        <div class="login-help">
            <p>Noch nicht registriert? 
                <a href="/ContractManagement-web/Controller?step=register">
                    Hier geht's zur Registrierung.
                </a>              
            </p>
        </div>
    </div>
</body>
</html>
